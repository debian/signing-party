-include /usr/share/dpkg/pkg-info.mk

DIRS=caff gpg-key2ps gpg-mailkeys gpgsigs gpglist gpgparticipants keyanalyze keylookup \
     sig2dot springgraph gpgwrap gpgdir keyart gpg-key2latex

all:
	for dir in $(DIRS) ; do if [ -f $$dir/Makefile ] ; then $(MAKE) -C $$dir || exit 1 ; fi ; done

install:
	for dir in $(DIRS) ; do if [ -f $$dir/Makefile ] ; then $(MAKE) -C $$dir install || exit 1 ; fi ; done

clean:
	for dir in $(DIRS) ; do if [ -f $$dir/Makefile ] ; then $(MAKE) -C $$dir clean || exit 1 ; fi ; done
