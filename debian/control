Source: signing-party
Section: misc
Priority: optional
Maintainer: Guilhem Moulin <guilhem@debian.org>
Uploaders: Simon Richter <sjr@debian.org>
Build-Depends: debhelper (>= 9), python, dh-python,
 autoconf, automake, autotools-dev,
 libmd-dev
Standards-Version: 4.1.4
Vcs-Git: https://salsa.debian.org/debian/signing-party.git
Vcs-Browser: https://salsa.debian.org/debian/signing-party

Package: signing-party
Architecture: any
Depends:
 ${perl:Depends},
 ${python:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
 gnupg,
 libclass-methodmaker-perl,
 libgnupg-interface-perl,
 libmailtools-perl,
 libmime-tools-perl,
 libnet-idn-encode-perl,
 libterm-readkey-perl,
 libtext-template-perl,
 qprint
Recommends:
 default-mta | mail-transport-agent,
 dialog | whiptail,
 libgd-gd2-noxpm-perl | libgd-gd2-perl,
 libpaper-utils
Suggests:
 fonts-noto-cjk,
 fonts-noto-mono,
 imagemagick | graphicsmagick-imagemagick-compat,
 mutt | neomutt,
 qrencode,
 texlive-font-utils,
 texlive-latex-extra,
 texlive-latex-recommended,
 texlive-xetex,
 wipe
Provides: sig2dot, springgraph, keyanalyze
Description: Various OpenPGP related tools
 signing-party is a collection for all kinds of PGP/GnuPG related things,
 including tools for signing keys, keyring analysis, and party preparation.
 .
  * caff: CA - Fire and Forget signs and mails a key
  * pgp-clean: removes all non-self signatures from key
  * pgp-fixkey: removes broken packets from keys
  * gpg-mailkeys: simply mail out a signed key to its owner
  * gpg-key2ps: generate PostScript file with fingerprint paper slips
  * gpgdir: recursive directory encryption tool
  * gpglist: show who signed which of your UIDs
  * gpgsigs: annotates list of GnuPG keys with already done signatures
  * gpgparticipants: create list of party participants for the organiser
  * gpgwrap: a passphrase wrapper
  * keyanalyze: minimum signing distance (MSD) analysis on keyrings
  * keylookup: ncurses wrapper around gpg --search
  * sig2dot: converts a list of GnuPG signatures to a .dot file
  * springgraph: creates a graph from a .dot file
  * keyart: creates a random ASCII art of a PGP key file
  * gpg-key2latex: generate LaTeX file with fingerprint paper slips
